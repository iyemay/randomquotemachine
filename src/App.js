import React from 'react';
import './App.scss';

const quotes = ["Elvis Has Left The Building", "Curiosity Killed The Cat",
    "Give a Man a Fish", "It's Not All It's Cracked Up To Be",
    "Talk the Talk"];

const authors = ["Janine Cook", "Johanna Shaw", "Maggie Carey",
    "Ron Hess", "Josue Rodriguez"];

class QuoteComponent extends React.Component {

    constructor(props) {
        super(props);

        const randomNumber = Math.floor(Math.random() * 5 );

        this.state = {
            inputQuote: quotes[randomNumber],
            inputAuthor: authors[randomNumber]
        };
        this.changeQuote = this.changeQuote.bind(this);
    }

    changeQuote() {
        const randomNumber = Math.floor(Math.random() * 5 );
        this.setState({
            inputQuote: quotes[randomNumber],
            inputAuthor: authors[randomNumber]
        });
    }

    render() {


        return (
            <div className="d-flex flex-column py-5 mt-3 my-md-5 align-self-center
                            justify-content-center align-items-center">

                <strong><p className="my-3 quote-header">Quotes</p></strong>

                <div id="quote-box" className="my-2 px-5 align-items-center align-content-center
                                                   justify-content-center">

                    <blockquote className="blockquote text-center">

                        <p id="text" className="">{this.state.inputQuote}</p>
                        <footer id="author" className="blockquote-footer">
                            {this.state.inputAuthor}
                        </footer>

                    </blockquote>

                    <div className="d-flex flex-md-row align-items-center justify-content-center align-content-center">

                        <button id="new-quote" className="btn "
                                onClick={this.changeQuote}>
                            <strong>New Quote</strong>
                        </button>


                        <a id="tweet-quote" href={`https://twitter.com/intent/tweet?text=${this.state.inputQuote}+
                            ${this.state.inputAuthor}`} className="">
                            <strong>Tweet it!</strong>
                        </a>
                    </div>

                </div>

            </div>
        );
    }
}

export default QuoteComponent;







